package jp.alhinc.cms.service.me;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.alhinc.cms.entity.User;
import jp.alhinc.cms.form.me.ChangePasswordForm;
import jp.alhinc.cms.mapper.UserMapper;

@Service
public class ChangePasswordService {

	@Autowired
	private UserMapper mapper;

	@Autowired
	PasswordEncoder passwordEncoder;

	@Transactional
	public int updatePassword(Long id, ChangePasswordForm form) {
		User target = mapper.findById(id);
		System.out.println("fffffffff");

		BCryptPasswordEncoder bccrypt = new BCryptPasswordEncoder();

		//入力した文字列とDBのハッシュ値の認証
		if(!bccrypt.matches(form.getCurrentPassword(),target.getPassword())){
			System.out.println("unmach-!");
			//----入力した現在のパスが間違っていたら０を返す。----
			return 0;

		}else {
			System.out.println("mach-!");	

			return mapper.updatePassword(id, new BCryptPasswordEncoder().encode(form.getNewPassword()),form.getLastPasswordChangedDate());
		}
	}
}