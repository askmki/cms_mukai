package jp.alhinc.cms.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/login")
public class LoginController {

	@GetMapping
	public String get(Model model) {
		model.addAttribute("isError", false);
		return "login";
	}

	@GetMapping("/error")
	public String error(Model model) {
		model.addAttribute("isError", true);
		return "login";
	}
}
