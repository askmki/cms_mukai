package jp.alhinc.cms.validator.user;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * ユーザのパスワードのバリデータクラス.
 *
 * @see <a href="https://docs.google.com/spreadsheets/d/1LXB05-HvAZ_guNMqfUIvb-mwL_OLiUJjWx7fZrQgPxg/edit#gid=895466845">機能仕様/パスワード変更</a>
 */
public class PasswordValidator implements ConstraintValidator<Password, String> {

	/** パスワードの正規表現パターン */
	//英大文字、英小文字を含んでいるかは肯定先読みで判定する
	private static final String AVILLABLE_REGEXP = "^(?=.*[A-Z])(?=.*[a-z])[a-zA-Z0-9!\"#$%&‘()*+,-./:;<=>?@[￥]^_`{|}~]{8,20}$";

	@Override
	public boolean isValid(String password, ConstraintValidatorContext context) {
		//数字または記号が含まれているかの確認
		if  ((password != null) && password.matches(AVILLABLE_REGEXP)){
			return((password.matches(".*[0-9!\\\"#$%&‘()*+,-./:;<=>?@[￥]^_`{|}~].*$"))); 
		}
		return true;
	}
}


