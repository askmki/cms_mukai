# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: localhost (MySQL 5.7.25)
# Database: cms
# Generation Time: 2020-12-07 10:18:56 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table branches
# ------------------------------------------------------------

DROP TABLE IF EXISTS `branches`;

CREATE TABLE `branches` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT COMMENT '管理通番',
  `code` char(3) NOT NULL DEFAULT '' COMMENT '店番号',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '本支店名',
  `name_kana` varchar(255) NOT NULL DEFAULT '' COMMENT '本支店名（カナ）',
  `created_user_id` char(9) NOT NULL DEFAULT '' COMMENT '作成者',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '作成日時',
  `updated_user_id` char(9) NOT NULL DEFAULT '' COMMENT '更新者',
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日時',
  `deleted_user_id` char(3) DEFAULT NULL COMMENT '削除者',
  `deleted_at` datetime DEFAULT NULL COMMENT '削除日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `branches` WRITE;
/*!40000 ALTER TABLE `branches` DISABLE KEYS */;

INSERT INTO `branches` (`id`, `code`, `name`, `name_kana`, `created_user_id`, `created_at`, `updated_user_id`, `updated_at`, `deleted_user_id`, `deleted_at`)
VALUES
	(1,'001','本社','ホンシャ','ADM_00001','2020-12-07 17:55:11','ADM_00001','2020-12-07 17:55:15',NULL,NULL);

/*!40000 ALTER TABLE `branches` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table departments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `departments`;

CREATE TABLE `departments` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT COMMENT '管理通番',
  `code` char(3) NOT NULL DEFAULT '' COMMENT '部署コード',
  `name` varchar(100) NOT NULL DEFAULT '' COMMENT '部署名',
  `name_kana` varchar(255) NOT NULL DEFAULT '' COMMENT '部署名（カナ）',
  `created_user_id` char(9) NOT NULL DEFAULT '' COMMENT '作成者',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '作成日時',
  `updated_user_id` char(9) NOT NULL DEFAULT '' COMMENT '更新者',
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日時',
  `deleted_user_id` char(9) DEFAULT NULL COMMENT '削除者',
  `deleted_at` datetime DEFAULT NULL COMMENT '削除日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `departments` WRITE;
/*!40000 ALTER TABLE `departments` DISABLE KEYS */;

INSERT INTO `departments` (`id`, `code`, `name`, `name_kana`, `created_user_id`, `created_at`, `updated_user_id`, `updated_at`, `deleted_user_id`, `deleted_at`)
VALUES
	(1,'001','情シス','ジョウシス','ADM_00001','2020-12-07 17:45:45','ADM_00001','2020-12-07 17:56:08',NULL,NULL);

/*!40000 ALTER TABLE `departments` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table positions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `positions`;

CREATE TABLE `positions` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT COMMENT '管理通番',
  `code` char(3) NOT NULL DEFAULT '' COMMENT '役職コード',
  `name` varchar(100) NOT NULL DEFAULT '' COMMENT '役職名',
  `name_kana` varchar(255) NOT NULL DEFAULT '' COMMENT '役職名（カナ）',
  `created_user_id` char(9) NOT NULL DEFAULT '' COMMENT '作成者',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '作成日時',
  `updated_user_id` char(9) NOT NULL DEFAULT '' COMMENT '更新者',
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日時',
  `deleted_user_id` char(9) DEFAULT NULL COMMENT '削除者',
  `deleted_at` datetime DEFAULT NULL COMMENT '削除日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `positions` WRITE;
/*!40000 ALTER TABLE `positions` DISABLE KEYS */;

INSERT INTO `positions` (`id`, `code`, `name`, `name_kana`, `created_user_id`, `created_at`, `updated_user_id`, `updated_at`, `deleted_user_id`, `deleted_at`)
VALUES
	(1,'001','管理者','カンリシャ','ADM_00001','2020-12-07 17:48:33','ADM_00001','2020-12-07 17:56:41',NULL,NULL);

/*!40000 ALTER TABLE `positions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `department_code` char(3) NOT NULL DEFAULT '',
  `position_code` char(3) NOT NULL DEFAULT '',
  `value` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;

INSERT INTO `roles` (`id`, `department_code`, `position_code`, `value`)
VALUES
	(1,'001','001','CUSTOMER_SERVICE'),
	(2,'002','001','CUSTOMER_SERVICE'),
	(3,'001','002','BACK_OFFICE'),
	(4,'002','002','BACK_OFFICE'),
	(5,'001','001','HOGE');

/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT COMMENT '管理通番',
  `login_id` char(9) NOT NULL DEFAULT '' COMMENT 'ログインID',
  `name` varchar(100) NOT NULL DEFAULT '' COMMENT 'ユーザ名',
  `name_kana` varchar(255) NOT NULL DEFAULT '' COMMENT 'ユーザ名（カナ）',
  `password` varchar(255) NOT NULL DEFAULT '' COMMENT '暗号化パスワード',
  `email` varchar(50) NOT NULL DEFAULT '' COMMENT 'メールアドレス',
  `branch_code` char(3) NOT NULL DEFAULT '' COMMENT '本支店コード',
  `department_code` char(3) NOT NULL DEFAULT '' COMMENT '所属部署コード',
  `position_code` char(3) NOT NULL DEFAULT '' COMMENT '役職コード',
  `authority_code` char(3) NOT NULL DEFAULT '' COMMENT '権限コード',
  `login_failed_count` int(2) NOT NULL DEFAULT '0' COMMENT 'ログイン失敗回数',
  `last_password_changed_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '最終パスワード更新日',
  `last_login_date` datetime DEFAULT NULL COMMENT '最終ログイン日時',
  `last_logout_date` datetime DEFAULT NULL COMMENT '最終ログアウト日時',
  `created_user_id` char(9) NOT NULL DEFAULT '' COMMENT '作成者',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '作成日時',
  `updated_user_id` char(9) NOT NULL DEFAULT '' COMMENT '更新者',
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新日時',
  `deleted_user_id` char(9) DEFAULT NULL COMMENT '削除者',
  `deleted_at` datetime DEFAULT NULL COMMENT '削除日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
