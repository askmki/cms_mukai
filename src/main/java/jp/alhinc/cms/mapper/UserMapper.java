package jp.alhinc.cms.mapper;

import java.sql.Timestamp;

import org.apache.ibatis.annotations.Mapper;

import jp.alhinc.cms.entity.User;

@Mapper
public interface UserMapper {

	/**
	 * IDに該当するユーザーを返却します.
	 * @param id ID
	 * @return IDに該当するユーザー
	 */
	User findById(Long id);

	/**
	 * ログインIDに該当するユーザーを返却します.
	 * @param loginId ログインID
	 * @return ログインIDに該当するユーザー
	 */
	User findByLoginId(String loginId);

	/**
	 * ログインユーザーの情報を返却します.
	 * @param loginId ログインID
	 * @return ログインユーザー情報
	 */
	User getLoginUserItem(String loginId);

	/**
	 * ユーザーのInsertを行います.
	 * @param user ユーザー情報
	 * @return 登録件数
	 */
	int insert(User user);

	/**
	 * パスワードの更新を行います.
	 * @param id ユーザーのID
	 * @param newEncryptedPassword 暗号化済みパスワード
	 * @param timestamp 
	 * @return 更新件数
	 */
	int updatePassword(Long id, String newEncryptedPassword, Timestamp lastPasswordChangedDate);

	/**
	 * ログイン失敗回数をリセットします.
	 * @param loginId ログインID
	 * @return 更新件数
	 */
	int resetLoginFailedCount(String loginId);

	/**
	 * ログイン失敗回数をカウントアップします.
	 * @param loginId ログインID
	 * @return 更新件数
	 */
	int countUpLoginFailedCount(String loginId);

}
