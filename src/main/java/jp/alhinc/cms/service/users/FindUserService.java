package jp.alhinc.cms.service.users;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.alhinc.cms.entity.User;
import jp.alhinc.cms.mapper.UserMapper;

@Service
public class FindUserService {

	@Autowired
	private UserMapper mapper;

	public User findByLoginId(String loginId) {
		return mapper.findByLoginId(loginId);
	}
}
