package jp.alhinc.cms.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.alhinc.cms.common.MessageId;
import jp.alhinc.cms.entity.User;
import jp.alhinc.cms.form.me.ChangePasswordForm;
import jp.alhinc.cms.service.me.ChangePasswordService;
import jp.alhinc.cms.util.MessageUtil;

@Controller
@RequestMapping("/me")
public class MeController extends AuthenticatedController {

	@Autowired
	private ChangePasswordService service;

	@GetMapping("/password")
	public String password(Model model) {
		model.addAttribute("form", new ChangePasswordForm());
		return "me/password";
	}

	@PatchMapping("/password")
	public String changePassword(@Valid @ModelAttribute("form") ChangePasswordForm form, BindingResult result, Model model) {
		User currentUser = getCurrentUser();
		System.out.println("currentpass" + currentUser.getPassword());
		System.out.println("current" + form.getCurrentPassword());
		System.out.println("new" + form.getNewPassword());
		System.out.println("confirm" + form.getPasswordConfirmation());
		
		//formでのエラーチェック
		if(result.hasErrors()) {
			model.addAttribute("form", form);
			return "me/password";
			
		} else {
		//上記をクリアした場合に次のエラーチェック（ーー入力した現在のパスワードが一致しなかった場合ーー）に入る
		if (service.updatePassword(currentUser.getId(), form) != 1) {
			System.out.println("cccccccccc");

			String error = MessageUtil.getMessage(MessageId.INF_010);
			model.addAttribute("error", error);
			return "me/password";

			}
		}
		System.out.println("dddddddddd");
		return "redirect:/me/password";
	}
}
