package jp.alhinc.cms.form.me;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import jp.alhinc.cms.common.MessageId;
import jp.alhinc.cms.validator.user.Password;

public class ChangePasswordForm implements Serializable {

	@NotBlank(message = MessageId.INF_002)
	private String currentPassword;

	@NotBlank(message = MessageId.INF_002)
	@Pattern(regexp = "^[a-zA-Z0-9!\"#$%&‘()*+,-./:;<=>?@[￥]^_`{|}~]+$",message= MessageId.INF_005)
	@Size(min=8, max=20,message=MessageId.INF_012)
	//複数の文字種を含んでいるかどうかのチェック
	@Password(message= MessageId.INF_007)
	private String newPassword;

	@NotBlank(message = MessageId.INF_002)
	private String passwordConfirmation;
	
	private Timestamp lastPasswordChangedDate;//変更後パスの使用開始日時

	//変更前後で同じパスだった場合
	@AssertTrue(message = MessageId.INF_008)
	public boolean isChangePassValid() {
		if (currentPassword != null && newPassword != null) { 
			return !(currentPassword.equals(newPassword)) ;
		}
		return true;
	}
	//新しいパスワードと確認用パスワードが一致しない場合
	@AssertTrue(message = MessageId.INF_011)
	public boolean isCheckPassValid() {
		if (newPassword != null && passwordConfirmation != null) { 
			return (newPassword.equals(passwordConfirmation)) ;
		}
		return true;
	}
	/**
	 * @return currentPassword
	 */
	public String getCurrentPassword() {
		return currentPassword;
	}

	/**
	 * @param currentPassword セットする currentPassword
	 */
	public void setCurrentPassword(String currentPassword) {
		this.currentPassword = currentPassword;
	}

	/**
	 * @return newPassword
	 */
	public String getNewPassword() {
		return newPassword;
	}

	/**
	 * @param newPassword セットする newPassword
	 */
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	/**
	 * @return passwordConfirmation
	 */
	public String getPasswordConfirmation() {
		return passwordConfirmation;
	}

	/**
	 * @param passwordConfirmation セットする passwordConfirmation
	 */
	public void setPasswordConfirmation(String passwordConfirmation) {
		this.passwordConfirmation = passwordConfirmation;
	}
	public Timestamp getLastPasswordChangedDate() {
		return lastPasswordChangedDate;
	}
	public void setLastPasswordChangedDate(Timestamp lastPasswordChangedDate) {
		this.lastPasswordChangedDate = lastPasswordChangedDate;
	}
}
