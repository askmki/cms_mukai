package jp.alhinc.cms.service.users;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.alhinc.cms.entity.Branch;
import jp.alhinc.cms.entity.Department;
import jp.alhinc.cms.entity.Position;
import jp.alhinc.cms.entity.User;
import jp.alhinc.cms.form.RegisterUserForm;
import jp.alhinc.cms.mapper.UserMapper;

@Service
public class CreateUserService {

	@Autowired
	private UserMapper mapper;

	@Transactional
	public void create(RegisterUserForm form, String operatorUserId) {
		User entity = new User();
		entity.setLoginId(form.getLoginId());
		entity.setName(form.getName());
		entity.setNameKana(form.getNameKana());
		entity.setPassword(new BCryptPasswordEncoder().encode(form.getRawPassword()));
		entity.setEmail(form.getEmail());
		Branch branch = new Branch();
		branch.setCode(form.getBranchCode());
		entity.setBranch(branch);
		Department department = new Department();
		department.setCode(form.getDepartmentCode());
		entity.setDepartment(department);
		Position position = new Position();
		position.setCode(form.getPositionCode());
		entity.setPosition(position);
		entity.setCreatedUserId(operatorUserId);
		entity.setUpdatedUserId(operatorUserId);

		mapper.insert(entity);
	}
}
